<?php


namespace Terminalbd\BankReconciliationBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;


class BankBranchRepository extends EntityRepository
{
    public function getBranches($filterBy)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bank', 'bank');
        $qb->leftjoin('e.district', 'district')
            ->select('e.branchCode', 'e.branchName','e.id')
            ->addSelect('district.name as districtName')
            ->addSelect('bank.name AS bankName')
            ->where('e.status = 1')
            ->andWhere('bank.status = 1');
        if ($filterBy != null){
            $qb->andWhere('bank.id = :bankId')->setParameter('bankId', $filterBy['bank']->getId());
        }

        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }
    
    public function getAllBranchesForSpecificBank(FileUpload $file)
    {
        $qb = $this->createQueryBuilder('e')
            ->join('e.bank', 'bank')
            ->where('bank.id = :bankId')->setParameter('bankId', $file->getBank()->getId());
        return $qb->getQuery()->getResult();
    }

    public function getBranchNameByBankAndName($bank, $name)
    {
        $result = $this->createQueryBuilder('bank_branch')
            ->where('bank_branch.bank =:bank')
            ->andWhere('bank_branch.branchName LIKE :name')
            ->setParameter('bank', $bank)
            ->setParameter('name', '%'.$name.'%')
            ->getQuery()
            ->getResult();
        return $result;
    }

    public function getAllBranchForInlineEdit($bank){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bank', 'bank')
            ->select('e.branchCode', 'e.branchName','e.id')
            ->where('e.status = 1')
            ->andWhere('bank.status = 1');
        $qb->andWhere('bank.id = :bankId')->setParameter('bankId', $bank);
        $records = $qb->getQuery()->getArrayResult();

        $returnArray=[];
        foreach ($records as $result){
            $returnArray[$result['id']]=$result['branchCode'].'--'.$result['branchName'];
        }
        return $returnArray;
    }


    public function  getBranchSelect2($branchKey,$bank){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bank', 'bank');
        $qb->select('e.id as id','e.branchName as name','e.branchCode as code');
        $qb->where('bank.id = :bankId')->setParameter('bankId', $bank);

        if(!empty($branchKey)){
            $qb->andWhere(
//                $qb->expr()->orX(
                    $qb->expr()->like("e.branchName", "'%$branchKey%'")
//                )
            );
        }
        $results = $qb->getQuery()->getArrayResult();

        $returnArray = [];
        foreach ($results as $result){
            $returnArray[$result['id']]= $result['name']. ' ('.$result['code'].')';
        }
        return $returnArray;
    }

}