<?php


namespace Terminalbd\BankReconciliationBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;


class BankAccCodeRepository extends EntityRepository
{
    public function GetAllAccountCode(){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bank', 'bank');
        $qb->leftJoin('e.branch','branch');
        $qb->select('e.id','e.accountType','e.accountNo','e.accountCode');
        $qb->addSelect('bank.id','bank.name','bank.slug');
        $qb->addSelect('branch.branchName as branchName');
        $qb->where('e.status = :status')->setParameter('status', 1);
        $qb->orderBy('e.id');
        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }


    public function GetAccountCodeWithBranch(){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bank', 'bank');
        $qb->leftJoin('e.branch', 'branch');
        $qb->select('e.id','e.accountType','e.accountNo','e.accountCode');
        $qb->addSelect('bank.name as bankName','bank.slug');
        $qb->addSelect('branch.branchName as branchName');
        $qb->where('e.status = :status')->setParameter('status', 1);
        $qb->orderBy('e.id','DESC');
        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }

    public function CompanyWiseReconciliationData($transactionDate){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.bank','bank');
//        $qb->join('bank.transactions', 'bankTransaction');
//        $qb->join('bankTransaction.bankReconciliation', 'bankReconciliation');

        $qb->select('e.id as BankCodeId','e.accountType as CompanyType');
//        $qb->addSelect('SUM(bankReconciliation.transactionAmount) as transactionAmount');
        $qb->addSelect('bank.id as bankId','bank.name as bankName','bank.slug');
        $qb->where('e.status = :status')->setParameter('status', 1);

        $qb->orderBy('bank.name','ASC');
        $qb->orderBy('e.accountType','DESC');

        $records = $qb->getQuery()->getArrayResult();
        return $records;
    }
}