<?php


namespace Terminalbd\BankReconciliationBundle\Repository\Api;


use Doctrine\ORM\EntityRepository;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;

class ApiRepository extends EntityRepository
{
    public function getBankBranches()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from(BankBranch::class, 'bank_branch')
            ->join('bank_branch.bank', 'bank')
            ->select('bank.name AS bankName', 'bank.slug AS bankSlug')
            ->addSelect('bank_branch.branchCode', 'bank_branch.branchName', 'bank_branch.mobile AS bankMobile');
        
        return $qb->getQuery()->getArrayResult();
        
    }
}