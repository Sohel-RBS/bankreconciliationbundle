<?php


namespace Terminalbd\BankReconciliationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\BankReconciliationBundle\Repository\BankAccCodeRepository")
 * @UniqueEntity(fields={"bank","accountType"}, message=" Code already existing for this bank,Please try again.")
 * @UniqueEntity(fields={"accountCode"}, message="This Code already existing,Please try again.")
 * @ORM\Table(name="br_bank_account_code")
 */
class BankAccountCode
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable = true)
     */
    private $status;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Bank")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $bank;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankBranch")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $branch;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $accountType;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $accountNo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $accountCode;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     */
    public function setBank($bank): void
    {
        $this->bank = $bank;
    }

    /**
     * @return string
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * @param string $accountType
     */
    public function setAccountType(string $accountType): void
    {
        $this->accountType = $accountType;
    }

    /**
     * @return mixed
     */
    public function getAccountNo()
    {
        return $this->accountNo;
    }

    /**
     * @param mixed $accountNo
     */
    public function setAccountNo($accountNo): void
    {
        $this->accountNo = $accountNo;
    }

    /**
     * @return mixed
     */
    public function getAccountCode()
    {
        return $this->accountCode;
    }

    /**
     * @param mixed $accountCode
     */
    public function setAccountCode($accountCode): void
    {
        $this->accountCode = $accountCode;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param mixed $branch
     */
    public function setBranch($branch): void
    {
        $this->branch = $branch;
    }




}