<?php


namespace Terminalbd\BankReconciliationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\BankReconciliationBundle\Repository\ReconciliationRangeRepository")
 * @UniqueEntity(fields={"startRange","endRange"}, message="Range already existing,Please try again.")
 * @ORM\Table(name="br_reconciliation_range")
 */
class ReconciliationRange
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */

    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $startRange;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $endRange;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable = true)
     */
    private $status;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }


    /**
     * @return mixed
     */
    public function getStartRange()
    {
        return $this->startRange;
    }

    /**
     * @param string $startRange
     */
    public function setStartRange($startRange): void
    {
        $this->startRange = $startRange;
    }

    /**
     * @return mixed
     */
    public function getEndRange()
    {
        return $this->endRange;
    }

    /**
     * @param mixed $endRange
     */
    public function setEndRange($endRange): void
    {
        $this->endRange = $endRange;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }


    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

}