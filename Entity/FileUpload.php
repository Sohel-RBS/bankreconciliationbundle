<?php

namespace Terminalbd\BankReconciliationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\BankReconciliationBundle\Repository\FileUploadRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"bank","accountType","transactionDate"}, message="For this date, already file existing,Please try again.")
 * @ORM\Table(name="br_import_file")
 */

class FileUpload
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable = true)
     */
    protected $status = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileName;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $accountType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Bank", inversedBy="uploadedFile")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $bank;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankAccountCode", inversedBy="uploadedFile")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $BankAccountCode;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankTransaction", mappedBy="fileUpload")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $transactions;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\BankReconciliationBundle\Entity\Reconciliation", mappedBy="fileUpload")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $reconciliations;



    /**
     * @ORM\Column(type="date", nullable = true)
     */
    private $transactionDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updatedAt;

    /**
     * @var $uploadedBy
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="fileUpload")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $uploadedBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }


    /**
     * @return string
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * @param string $accountType
     */
    public function setAccountType(string $accountType): void
    {
        $this->accountType = $accountType;
    }




    /**
     * @return BankTransaction
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @param BankTransaction $transactions
     */
    public function setTransactions($transactions): void
    {
        $this->transactions = $transactions;
    }
    
    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     */
    public function setBank($bank): void
    {
        $this->bank = $bank;
    }


    /**
     * @return mixed
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * @param mixed $transactionDate
     */
    public function setTransactionDate($transactionDate): void
    {
        $this->transactionDate = $transactionDate;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getUploadedBy()
    {
        return $this->uploadedBy;
    }

    /**
     * @param mixed $uploadedBy
     */
    public function setUploadedBy($uploadedBy): void
    {
        $this->uploadedBy = $uploadedBy;
    }

    /**
     * @return Reconciliation
     */
    public function getReconciliations()
    {
        return $this->reconciliations;
    }


    /**
     * @param Reconciliation $reconciliation
     */
    public function setReconciliations($reconciliation): void
    {
        $this->reconciliations = $reconciliation;
    }

    /**
     * @return mixed
     */
    public function getBankAccountCode()
    {
        return $this->BankAccountCode;
    }

    /**
     * @param mixed $BankAccountCode
     */
    public function setBankAccountCode($BankAccountCode)
    {
        $this->BankAccountCode = $BankAccountCode;
    }








}