<?php


namespace Terminalbd\BankReconciliationBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;
use Terminalbd\BankReconciliationBundle\Entity\BankTransaction;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Terminalbd\BankReconciliationBundle\Form\FileUploadFormType;
use Symfony\Contracts\Translation\TranslatorInterface;


/**
 * Class FileUploadController
 * @package Terminalbd\BankReconciliationBundle\Controller
 * @Route("/file")
 * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_USER')")
 */
class FileUploadController extends AbstractController
{
    /**
     * @Route("/bank-statement-upload", name="br_bank_statement_upload")
     */
    public function bankStatementUpload(Request $request, TranslatorInterface $translator)
    {
//        dd($request);
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $fileUpload = new FileUpload();
        $allowFileType = ['xlsx'];

        $data = $this->getDoctrine()->getRepository(FileUpload::class)
            ->findBy(['type' => 'bank-statement'],['id' => 'desc']);
        $form = $this->createForm(FileUploadFormType::class, $fileUpload);
        $form->handleRequest($request);

        /*$exist = $this->getDoctrine()->getRepository(FileUpload::class)->findOneBy(array('bank'=>$fileUpload->getBank(),'accountType'=>$fileUpload->getAccountType(),'transactionDate'=>$fileUpload->getTransactionDate()));*/
        if ($form->isSubmitted() && $form->isValid()){
            $transactionDate = $fileUpload->getTransactionDate()->format('d-m-Y');
            /*if($exist){
                $this->addFlash('error', $translator->trans('File Already Uploaded!'));
                return $this->redirectToRoute('br_bank_statement_upload');
            }else{*/
                $uploadedFile = $form['UploadFile']->getData();
                if (in_array($uploadedFile->getClientOriginalExtension(), $allowFileType)){
                    $em = $this->getDoctrine()->getManager();
                    $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);

                    $newFileName = $form['bank']->getData()->getSlug() . '_TD_' . $transactionDate . '_CD_' . date('d-m-Y') . '-' . time() . '.' . $uploadedFile->getClientOriginalExtension();
                    $uploadDir = $this->get('kernel')->getProjectDir() . '/public/uploads/reconciliation/';
                    $uploadedFile->move(
                        $uploadDir,
                        $newFileName
                    );

                    $accountCode = $this->getDoctrine()->getRepository(BankAccountCode::class)->findOneBy(['bank'=>$form['bank']->getData()->getId(),'accountType'=>$form['accountType']->getData()]);
                    if ($accountCode) {
                        $code = $this->getDoctrine()->getRepository(BankAccountCode::class)->find($accountCode->getId());
                    }else{
                        $code = null;
                    }

                    $fileUpload->setFileName($newFileName);
                    $fileUpload->setType('bank-statement');
                    $fileUpload->setCreatedAt( new \DateTime('now'));
                    $fileUpload->setBankAccountCode($code);
                    $em->persist($fileUpload);
                    $em->flush();

                    $this->addFlash('success', $translator->trans('File Uploaded Successfully!'));
                    return $this->redirectToRoute('br_bank_statement_list');
                }else{
                    $this->addFlash('error', $translator->trans('Invalid File Format!'));
                    return $this->redirectToRoute('br_bank_statement_upload');
                }
//            }

        }
        return $this->render('@TerminalbdBankReconciliation/fileUpload/bank-statement.html.twig',[
            'form' => $form->createView(),
            'data' => $data,
        ]);
    }



    /**
     * @Route("/bank-statement-list", name="br_bank_statement_list")
     */
    public function bankStatementList(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $data = $this->getDoctrine()->getRepository(FileUpload::class)->findBy(['type' => 'bank-statement'],['id' => 'desc']);
//        $rData = $this->getDoctrine()->getRepository(FileUpload::class)->allUploadFileWithTotal();
//        dd($data,$rData);
        return $this->render('@TerminalbdBankReconciliation/fileUpload/bank-statement-list.html.twig',[
            'data' => $data,
        ]);
    }

    /**
     * @Route("/{id}/delete", name="br_file_delete")
     */
    public function delete(FileUpload $file)
    {
        $uploadDir = $this->get('kernel')->getProjectDir() . '/public/uploads/reconciliation/';

        $em = $this->getDoctrine()->getManager();
        $em->remove($file);
        $em->flush();

        if (file_exists($uploadDir.$file->getFileName())){
            $filesystem = new Filesystem();
            $filesystem->remove($uploadDir.$file->getFileName());
        }

        $this->addFlash('success','File has been deleted!');
        return $this->redirectToRoute('br_bank_statement_upload');
    }

    /**
     * @Route("/custom-statement-create", name="br_custom_statement_create")
     */
    public function customStatementCreate(Request $request)
    {
        $fileUpload = new FileUpload();
        $form = $this->createForm(FileUploadFormType::class,$fileUpload)->remove('UploadFile');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $accountCode = $this->getDoctrine()->getRepository(BankAccountCode::class)->findOneBy(['bank'=>$form['bank']->getData()->getId(),'accountType'=>$form['accountType']->getData()]);
            if ($accountCode) {
                $code = $this->getDoctrine()->getRepository(BankAccountCode::class)->find($accountCode->getId());
            }else{
                $code = null;
            }

            $transactionDate = $fileUpload->getTransactionDate()->format('d-m-Y');
            $em = $this->getDoctrine()->getManager();
            $fileUpload->setType('bank-statement');
            $fileUpload->setBankAccountCode($code);
            $fileUpload->setCreatedAt( new \DateTime('now'));
            $fileUpload->setStatus(1);
            $em->persist($fileUpload);
            $em->flush();

            $this->addFlash('success','Custom Statement Added Successfully!');
            return $this->redirectToRoute('br_bank_statement_list');
        }
        return $this->render('@TerminalbdBankReconciliation/fileUpload/custom-statement-create.html.twig',[
            'form' => $form->createView(),
        ]);
    }
}