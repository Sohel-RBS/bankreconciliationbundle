<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\BankReconciliationBundle\Controller;


use App\Entity\Admin\Bank;
use App\Entity\Core\Agent;
use App\Service\SmsSender;
use PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions\F;
use Proxies\__CG__\Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;
use Terminalbd\BankReconciliationBundle\Entity\BankTransaction;
use Terminalbd\BankReconciliationBundle\Entity\Reconciliation;
use Terminalbd\BankReconciliationBundle\Entity\SalesPayment;
use Terminalbd\BankReconciliationBundle\Form\BankTransactionFormType;
use Terminalbd\BankReconciliationBundle\Form\DateRangeFormType;
use Terminalbd\BankReconciliationBundle\Form\FileUploadFormType;
use Terminalbd\BankReconciliationBundle\Form\ReconciliationFormType;
use Terminalbd\BankReconciliationBundle\Form\SearchFormType;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Class BankReconciliationController
 * @package Terminalbd\BankReconciliationBundle\Controller
 * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_USER')")
 */
class BankReconciliationController extends AbstractController
{

    private function paginate(Request $request, $records)
    {
        $paginator  = $this->get('knp_paginator');
        return $paginator->paginate($records, $request->query->get('page', 1)/*page number*/, 25  /*limit per page*/
        );
    }


    /**
     * @param Request $request
     * @Route("/transaction/{id}/add", name="br_transaction_add")
     */
    public function transactionAdd(Request $request, \Terminalbd\BankReconciliationBundle\Entity\FileUpload $fileUpload)
    {
//        dd($request);
        $em = $this->getDoctrine()->getManager();
        $entity = new Reconciliation();
        $data = $request->request->all();
        $form = $this->createForm(ReconciliationFormType::class, $entity,array('fileUpload' => $fileUpload));
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            $entity->setFileUpload($fileUpload);
            $agentId = $data['reconciliation_form']['agentId'];
            $agent = $this->getDoctrine()->getRepository(Agent::class)->find($agentId);

            $entity->getBankTransaction()->setBranch($entity->getBranch());
            $entity->getBankTransaction()->setBank($entity->getBranch()->getBank());
            $entity->getBankTransaction()->setFileUpload($fileUpload);
            $entity->getBankTransaction()->setStatus(1);
            $entity->getBankTransaction()->setTransactionDate($fileUpload->getTransactionDate());
            $entity->getBankTransaction()->setReconciliationId($entity);

            if ($entity->getSalesPayment()->getDepositAmount() && $agent){
                $entity->getSalesPayment()->setBranch($entity->getBranch());
                $entity->setSalesPaymentAmount($entity->getSalesPayment()->getDepositAmount());
                $entity->setActualAmount($entity->getSalesPayment()->getDepositAmount());
                $entity->setDiffAmount($entity->getSalesPayment()->getDepositAmount()-$entity->getBankTransaction()->getDeposit());
                $entity->getSalesPayment()->setStatus(1);
                $entity->getSalesPayment()->setFileUpload($fileUpload);
                $entity->getSalesPayment()->setBankTransaction($entity->getBankTransaction());
                $entity->getSalesPayment()->setReconciliationId($entity);
                $entity->getSalesPayment()->setAgent($agent);
                $entity->setAgent($agent);
            }
            $entity->setTransactionAmount($entity->getBankTransaction()->getDeposit());

            $entity->getSalesPayment(null);
            $entity->setStatus(1);
            $entity->setIsCustom('yes');
            $entity->setReconciliationFlag('purple');
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('br_index', [
                'transactionDate' => date_format($fileUpload->getTransactionDate(),"d-m-Y"),
                'bank' => $fileUpload->getBank()->getSlug(),
                'accountType' => $fileUpload->getAccountType()
            ], 307);
        }

        return $this->render('@TerminalbdBankReconciliation/transaction/create.html.twig',[
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="br_index")
     */
    public function index(Request $request, ParameterBagInterface $parameterBag) {
        $statementData = [];
        $salesData = [];
        $salesSelect = [];
        $agentSelect = [];
        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);
        $uploadID = null;
        $reconciliationData = null;
        if ($form->isSubmitted()){
            $bank = $form['bank']->getData();
            $type = $form['accountType']->getData();
            $date = $form['transactionDate']->getData();
            $transactionDate = date("Y-m-d", strtotime($date));
            $transactionDate = new \DateTime($transactionDate);

            if ($bank && $transactionDate && $type){
                $uploadID = $this->getDoctrine()->getRepository(FileUpload::class)->findOneBy(['bank' => $bank, 'accountType' => $type,'transactionDate'=>$transactionDate]);

                $salesSelect = $this->getDoctrine()->getRepository(SalesPayment::class)->getSalesDropDownData($bank,$transactionDate,$type);
                $agentSelect = $this->getDoctrine()->getRepository(Agent::class)->findBy(['agentGroup' => 11,'status'=>1]);

                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->reconcialiationData($uploadID);
            }
        }

        if ($request->getMethod() == 'GET' && null != $request->query->get('accountType') || $request->getMethod() == 'POST' && null != $request->query->get('accountType')){
            $requestDate = $request->query->get('transactionDate');
            $requestDate = date("Y-m-d", strtotime($requestDate));
            $date = new \DateTime($requestDate);

            $bank = $request->query->get('bank');
            $type = $request->query->get('accountType');
            $form->get('accountType')->setData($type);
            $form->get('transactionDate')->setData(date("d-m-Y", strtotime($requestDate)));
            $getBankInfo = $this->getDoctrine()->getRepository('App:Admin\Bank')->findOneBy(['slug'=>$bank]);
            $form->get('bank')->setData($getBankInfo);

            if ($getBankInfo && $date && $type){
                $uploadID = $this->getDoctrine()->getRepository(FileUpload::class)->findOneBy(['bank' => $getBankInfo, 'accountType' => $type,'transactionDate'=>$date]);

                $salesSelect = $this->getDoctrine()->getRepository(SalesPayment::class)->getSalesDropDownData($getBankInfo,$date,$type);
                $agentSelect = $this->getDoctrine()->getRepository(Agent::class)->findBy(['agentGroup' => 11,'status'=>1]);

                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->reconcialiationData($uploadID);
            }
        }

//        $returnArray=[];
//        foreach ($findChickAgent as $result){
//            $returnArray[$result->getId()]= $result->getName().' ('.$result->getAgentId().')';
//        }

//        dd($agentSelect);

        return $this->render('@TerminalbdBankReconciliation/reconciliation/index.html.twig',[
            'form' => $form->createView(),
            'salesSelect' =>$salesSelect,
            'agentSelect' =>$agentSelect,
            'reconciliationData' => $reconciliationData,
        ]);
    }

    /**
     * @Route("/agent/send", name="br_send_sms_to_agent")
     */
    public function sendSmsToAgent(SmsSender $smsSender, Request $request)
    {
        $mobile = $request->query->get('mobile');

        /**
         * @var BankTransaction $id
         */
        $transactionId = $request->query->get('statementId');
        $adjustAmount = $request->query->get('adjustAmount');
        $indexNo = $request->query->get('indexno');
        $salesId = $request->query->get('salesid');
        $transactionSelect = $request->query->get('transactionSelect');
        $bankId = $request->query->get('bankId');
        $accountType = $request->query->get('accountType');
        $transactionDate = $request->query->get('transactionDate');
        $transactionDate = date("Y-m-d", strtotime($transactionDate));
        $transactionDate = new \DateTime($transactionDate);

        if ($transactionSelect == 0){
            $findReconciliation = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(['bankTransaction' => $transactionId]);
            if ($salesId && $findReconciliation && $adjustAmount >= 0) {
                if ($mobile) {
                    $mobile = str_replace('+88', '', $mobile);
                    $mobile = '01315559286';
                    $message = 'Thanks! Payment received.';
//                $smsSender->sendSmsToAgent($message, $mobile);
                } else {
                    $mobile = '';
                }
                if ($findReconciliation) {
                    if ($adjustAmount > 0) {
                        $findReconciliation->setSalesPaymentAmount($adjustAmount);
                    }
                    $findReconciliation->setReconciliationFlag('green');
                    $findReconciliation->setApprovedBy($this->getUser());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($findReconciliation);
                    $em->flush();
                }

                if ($bankId && $transactionDate && $accountType){
                    $uploadID = $this->getDoctrine()->getRepository(FileUpload::class)->findOneBy(['bank' => $bankId, 'accountType' => $accountType,'transactionDate'=>$transactionDate]);

                    $bank = $this->getDoctrine()->getRepository(Bank::class)->find($bankId);

                    $salesSelect = $this->getDoctrine()->getRepository(SalesPayment::class)->getSalesDropDownData($bank,$transactionDate,$accountType);
                    $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->reconcialiationData($uploadID);
                    $agentSelect = $this->getDoctrine()->getRepository(Agent::class)->findBy(['agentGroup' => 11,'status'=>1]);
                    $html = $this->renderView(
                        '@TerminalbdBankReconciliation/reconciliation/load-data.html.twig', array(
                            'salesSelect' =>$salesSelect,
                            'reconciliationData'=>$reconciliationData,
                            'agentSelect' =>$agentSelect,
                        )
                    );
                }


                return new JsonResponse([
                    'message' => 'success',
                    'mobile' => $mobile,
                    'contentHtml' => $html
                ]);
            } else {
                return new JsonResponse([
                    'index' => $indexNo,
                    'message' => 'failed'
                ]);
            }
        }else{
            $this->ReconciliationBySelectTransaction($salesId,$transactionId);
            $findReconciliation = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(['bankTransaction' => $transactionId]);
            $findAgent = $this->getDoctrine()->getRepository(Agent::class)->find($findReconciliation->getAgent());
            $mobile = $findAgent->getMobile();

            if ($salesId && $findReconciliation && $adjustAmount >= 0) {
                if ($mobile) {
                    $mobile = str_replace('+88', '', $mobile);
                    $mobile = '01729762344';
                    $message = 'Thanks! Payment received.';
//                    $smsSender->sendSmsToAgent($message, $mobile);
                } else {
                    $mobile = '';
                }
                if ($findReconciliation) {
                    $findReconciliation->setApprovedBy($this->getUser());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($findReconciliation);
                    $em->flush();
                }
                if ($bankId && $transactionDate && $accountType){
                    $uploadID = $this->getDoctrine()->getRepository(FileUpload::class)->findOneBy(['bank' => $bankId, 'accountType' => $accountType,'transactionDate'=>$transactionDate]);

                    $bank = $this->getDoctrine()->getRepository(Bank::class)->find($bankId);

                    $salesSelect = $this->getDoctrine()->getRepository(SalesPayment::class)->getSalesDropDownData($bank,$transactionDate,$accountType);
                    $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->reconcialiationData($uploadID);
                    $agentSelect = $this->getDoctrine()->getRepository(Agent::class)->findBy(['agentGroup' => 11,'status'=>1]);

                    $html = $this->renderView(
                        '@TerminalbdBankReconciliation/reconciliation/load-data.html.twig', array(
                            'salesSelect' =>$salesSelect,
                            'reconciliationData'=>$reconciliationData,
                            'agentSelect'=>$agentSelect,
                        )
                    );
                }
                return new JsonResponse([
                    'message' => 'success',
                    'mobile' => $mobile,
                    'contentHtml' => $html
                ]);
            } else {
                return new JsonResponse([
                    'index' => $indexNo,
                    'message' => 'failed'
                ]);
            }
        }
    }

    /**
     * @Route("/agent/send/multiple", name="br_send_sms_to_agent_multiple")
     */
    public function sendSmsToAgentMultiple(SmsSender $smsSender, Request $request)
    {
        /**
         * @var BankTransaction $id
         */
        $data = $request->query->get('data');
        $bankId = $request->query->get('bankId');
        $accountType = $request->query->get('accountType');
        $transactionDate = $request->query->get('transactionDate');
        $transactionDate = date("Y-m-d", strtotime($transactionDate));
        $transactionDate = new \DateTime($transactionDate);
        foreach ($data as $row){
            $findReconciliation = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(['bankTransaction' => $row['TransactionId']]);
            if ($row['SalesID'] && $findReconciliation && $row['adjustAmount'] >= 0) {
                if ($row['mobile']) {
                    $mobile = str_replace('+88', '', $row['mobile']);
                    $mobile = '01315559286';
                    $message = 'Thanks! Payment received.';
//                $smsSender->sendSmsToAgent($message, $mobile);
                } else {
                    $mobile = '';
                }
                if ($findReconciliation) {
                    if ($row['adjustAmount'] > 0) {
                        $findReconciliation->setSalesPaymentAmount($row['adjustAmount']);
                    }
                    $findReconciliation->setReconciliationFlag('green');
                    $findReconciliation->setApprovedBy($this->getUser());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($findReconciliation);
                    $em->flush();
                }
            }
        }

        if ($bankId && $transactionDate && $accountType){
            $uploadID = $this->getDoctrine()->getRepository(FileUpload::class)->findOneBy(['bank' => $bankId, 'accountType' => $accountType,'transactionDate'=>$transactionDate]);

            $bank = $this->getDoctrine()->getRepository(Bank::class)->find($bankId);

            $salesSelect = $this->getDoctrine()->getRepository(SalesPayment::class)->getSalesDropDownData($bank,$transactionDate,$accountType);
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->reconcialiationData($uploadID);
            $agentSelect = $this->getDoctrine()->getRepository(Agent::class)->findBy(['agentGroup' => 11,'status'=>1]);

            $html = $this->renderView(
                '@TerminalbdBankReconciliation/reconciliation/load-data.html.twig', array(
                    'salesSelect' => $salesSelect,
                    'reconciliationData' => $reconciliationData,
                    'agentSelect' => $agentSelect,
                )
            );
        }
        return new JsonResponse([
            'message' => 'success',
            'mobile' => $mobile,
            'contentHtml' => $html
        ]);
    }

    public function ReconciliationBySelectTransaction($salesId,$transactionId){
        $em = $this->getDoctrine()->getManager();
        $bankReconciliation = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(array('bankTransaction'=>$transactionId));

        $salesDataDelete = $this->getDoctrine()->getRepository(Reconciliation::class)->findOneBy(['salesPayment'=>$salesId]);
        $em->remove($salesDataDelete);
        $em->flush();

        $salesData = $this->getDoctrine()->getRepository(SalesPayment::class)->find($salesId);

        $bankReconciliation->setSalesPayment($salesData);
        $bankReconciliation->setBranch($salesData->getBranch());
        $bankReconciliation->setStatus(1);
//        $bankReconciliation->setSalesPaymentAmount($bankReconciliation->getTransactionAmount());
        $bankReconciliation->setSalesPaymentAmount($salesData->getDepositAmount());
        $bankReconciliation->setDiffAmount($salesData->getDepositAmount() - $bankReconciliation->getTransactionAmount());
        $bankReconciliation->setUpdatedAt(new \DateTime('now'));
        $bankReconciliation->setAgent($salesData->getAgent());
        $bankReconciliation->setActualAmount($salesData->getDepositAmount());
        $bankReconciliation->setReconciliationFlag('green');
        $bankReconciliation->setIsCustom('yes');

        $em->persist($bankReconciliation);
        $em->flush();

        $salesData->setReconciliationId($bankReconciliation);
        $salesData->setBankTransaction($bankReconciliation->getBankTransaction());
        $em->persist($salesData);
        $em->flush();
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/daily/report", name="br_reconciliation_daily_report")
     */
    public function reconciliationDailyReprt(Request $request){
        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);

        $reconciliationData = [];
        $headingMsg = '';
        $type = '';
        $bank = '';
        $transactionDate = '';
        if ($form->isSubmitted()){
            $date = $form['transactionDate']->getData();
            $transactionDate = date("Y-m-d", strtotime($date));
            $transactionDate = new \DateTime($date);
            $type = $form['accountType']->getData();
            $bank = $form['bank']->getData();
//            $type = null;
            if ($bank == null){
                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationReportData($type,$transactionDate);
            }else{
                $uploadID = $this->getDoctrine()->getRepository(\Proxies\__CG__\Terminalbd\BankReconciliationBundle\Entity\FileUpload::class)->findOneBy(['bank' => $bank, 'accountType' => $type,'transactionDate'=>$transactionDate]);

                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationReportDataByUploadID($uploadID);
            }

            if ($type == 'AGRO'){
                $headingMsg = 'NOURISH AGRO LIMITED';
            }
            if ($type == 'POULTRY'){
                $headingMsg = 'NOURISH POULTRY & HATCHERY LIMITED';
            }
            if ($type == 'FEED'){
                $headingMsg = 'NOURISH FEEDS LIMITED';
            }
        }

        return $this->render('@TerminalbdBankReconciliation/report/reconciliation-daily-report.html.twig',[
            'form' => $form->createView(),
            'reconciliationData' => $reconciliationData,
            'headingMsg' => $headingMsg,
            'type'=> $type,
            'bank' => $bank,
            'transactionDate' => $transactionDate,
        ]);
    }



    /**
     * @param Reconciliation $reconciliation
     * @Route("/chick/agent/{id}/reconciliation", name="br_chick_reconciliation")
     */
    public function reconciliationChickAgent(SmsSender $smsSender,Request $request, Reconciliation $reconciliation)
    {
        $em = $this->getDoctrine()->getManager();

        $data = $request->request->all();
        $findAgent = $this->getDoctrine()->getRepository(Agent::class)->find($_GET['agentId']);
        $transactionData = $this->getDoctrine()->getRepository(BankTransaction::class)->find($reconciliation->getBankTransaction());
        $chickSalesEntity = new SalesPayment();
        $chickSalesEntity->setAgent($findAgent);
        $chickSalesEntity->setBranch($transactionData->getBranch());
        $chickSalesEntity->setDepositAmount($reconciliation->getTransactionAmount());
        $chickSalesEntity->setDepositDate($transactionData->getTransactionDate());
        $chickSalesEntity->setStatus(true);
        $chickSalesEntity->setFileUpload($transactionData->getFileUpload());
        $chickSalesEntity->setReconciliationId($reconciliation);
        $chickSalesEntity->setBankTransaction($transactionData);
        $em->persist($chickSalesEntity);
        $em->flush();

        $reconciliation->setSalesPayment($chickSalesEntity);
        $reconciliation->setSalesPaymentAmount($reconciliation->getTransactionAmount());
        $reconciliation->setDiffAmount(0);
        $reconciliation->setAgent($findAgent);
        $reconciliation->setActualAmount($reconciliation->getTransactionAmount());
        $reconciliation->setReconciliationFlag('teal');
        $reconciliation->setApprovedBy($this->getUser());
        $reconciliation->setMode('Chick');
        $reconciliation->setIsCustom('yes');
        $em->persist($reconciliation);
        $em->flush();

        if ($findAgent->getMobile()){
//            $mobile = str_replace('+88', '', $findAgent->getMobile());
            $mobile = '01729762344';
            $message = 'Thanks! Payment received.';
//            $smsSender->sendSmsToAgent($message, $mobile);
        }

        $bank = $reconciliation->getBankTransaction()->getBank();
        $transactionDate = $reconciliation->getBankTransaction()->getTransactionDate();
        $accountType = $reconciliation->getFileUpload()->getAccountType();
        $uploadID = $reconciliation->getFileUpload()->getId();

        $salesSelect = $this->getDoctrine()->getRepository(SalesPayment::class)->getSalesDropDownData($bank,$transactionDate,$accountType);
        $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->reconcialiationData($uploadID);
        $agentSelect = $this->getDoctrine()->getRepository(Agent::class)->findBy(['agentGroup' => 11,'status'=>1]);

        $html = $this->renderView(
            '@TerminalbdBankReconciliation/reconciliation/load-data.html.twig', array(
                'salesSelect' =>$salesSelect,
                'reconciliationData'=>$reconciliationData,
                'agentSelect'=>$agentSelect,
            )
        );
//        return new JsonResponse(['status' => 200]);
        return new JsonResponse([
            'message' => '200',
            'mobile' => $mobile,
            'contentHtml' => $html
        ]);
    }



    /**
     * @param Reconciliation $reconciliation
     * @Route("/chick/feed/agent/{id}/reconciliation", name="br_chick_feed_reconciliation")
     */
    public function reconciliationChickFeedAgent(SmsSender $smsSender,Request $request, Reconciliation $reconciliation)
    {
        $em = $this->getDoctrine()->getManager();

        $findAgent = $this->getDoctrine()->getRepository(Agent::class)->find($_GET['agentId']);
        $transactionData = $this->getDoctrine()->getRepository(BankTransaction::class)->find($reconciliation->getBankTransaction());
        /*if ($transactionData->getBranch() == null){
            dd($findAgent->get);
        }
        dd($transactionData->getBank(),$transactionData->getBranch());*/
        $chickSalesEntity = new SalesPayment();
        $chickSalesEntity->setAgent($findAgent);
        $chickSalesEntity->setBranch($transactionData->getBranch());
        $chickSalesEntity->setDepositAmount($reconciliation->getTransactionAmount());
        $chickSalesEntity->setDepositDate($transactionData->getTransactionDate());
        $chickSalesEntity->setStatus(true);
        $chickSalesEntity->setFileUpload($transactionData->getFileUpload());
        $chickSalesEntity->setReconciliationId($reconciliation);
        $chickSalesEntity->setBankTransaction($transactionData);
        $em->persist($chickSalesEntity);
        $em->flush();

        $reconciliation->setSalesPayment($chickSalesEntity);
        $reconciliation->setSalesPaymentAmount($reconciliation->getTransactionAmount());
        $reconciliation->setDiffAmount(0);
        $reconciliation->setAgent($findAgent);
        $reconciliation->setActualAmount($reconciliation->getTransactionAmount());
        $reconciliation->setReconciliationFlag('teal');
        $reconciliation->setApprovedBy($this->getUser());
//        $reconciliation->setMode('Chick');
        $reconciliation->setIsCustom('yes');
        $em->persist($reconciliation);
        $em->flush();

        if($reconciliation->getMode() == 'Feed'){
            $transactionData->setIsAdvance('yes');
            $em->persist($transactionData);
            $em->flush();
        }

        if ($findAgent->getMobile()){
//            $mobile = str_replace('+88', '', $findAgent->getMobile());
            $mobile = '01729762344';
            $message = 'Thanks! Payment received.';
//            $smsSender->sendSmsToAgent($message, $mobile);
        }


        $bank = $reconciliation->getFileUpload()->getBank();
        $type = $reconciliation->getFileUpload()->getAccountType();
        $transactionDate = $reconciliation->getFileUpload()->getTransactionDate();

        $uploadID = $this->getDoctrine()->getRepository(\Proxies\__CG__\Terminalbd\BankReconciliationBundle\Entity\FileUpload::class)->findOneBy(['bank' => $bank, 'accountType' => $type,'transactionDate'=>$transactionDate]);
        $records = $this->getDoctrine()->getRepository(BankTransaction::class)->findBy(['fileUpload'=>$uploadID],['id'=>'DESC']);
        $html = $this->renderView(
            '@TerminalbdBankReconciliation/transaction/load-data.html.twig', array(
                'transactionData' =>$records
            )
        );
        return new JsonResponse([
            'message' => '200',
            'mobile' => $mobile,
            'contentHtml' => $html
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/daily/report/excel", name="br_reconciliation_daily_report_excel")
     */
    public function reconciliationDailyReprtExcel(Request $request){
        $type = $request->query->get('type');
        $bank = $request->query->get('bank');
        $date = $request->query->get('transactionDate');

        if ($bank == null){
            $transactionDate = date("Y-m-d", strtotime($date));
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationReportData($type,$transactionDate);
        }else{
            $transactionDate = date("Y-m-d", strtotime($date));
            $transactionDate = new \DateTime($date);

            $uploadID = $this->getDoctrine()->getRepository(\Proxies\__CG__\Terminalbd\BankReconciliationBundle\Entity\FileUpload::class)->findOneBy(['bank' => $bank, 'accountType' => $type,'transactionDate'=>$transactionDate]);

            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationReportDataByUploadID($uploadID);
            $transactionDate = date("Y-m-d", strtotime($date));
        }

        if ($type == 'AGRO'){
            $headingMsg = 'NOURISH AGRO LIMITED';
        }
        if ($type == 'POULTRY'){
            $headingMsg = 'NOURISH POULTRY & HATCHERY LIMITED';
        }
        if ($type == 'FEED'){
            $headingMsg = 'NOURISH FEEDS LIMITED';
        }

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-daily-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'headingMsg' => $headingMsg,
            'type' =>$type,
        ]);
        if ($bank == null){
            $fileName = $type.'_'.$transactionDate.'_'.time().".xls";
        }else{
            $bankData = $this->getDoctrine()->getRepository(Bank::class)->find($bank);
            $fileName = $bankData->getSlug().'_'.$type.'_'.$transactionDate.'_'.time().".xls";
        }

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$fileName");

        echo $html;
        die;
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/daily/report/pdf", name="br_reconciliation_daily_report_pdf")
     */
    public function reconciliationDailyReprtPdf(Request $request){
        $type = $request->query->get('type');
        $bank = $request->query->get('bank');
        $date = $request->query->get('transactionDate');

        if ($bank == null){
            $transactionDate = date("Y-m-d", strtotime($date));
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationReportData($type,$transactionDate);
        }else{
            $transactionDate = date("Y-m-d", strtotime($date));
            $transactionDate = new \DateTime($date);

            $uploadID = $this->getDoctrine()->getRepository(\Proxies\__CG__\Terminalbd\BankReconciliationBundle\Entity\FileUpload::class)->findOneBy(['bank' => $bank, 'accountType' => $type,'transactionDate'=>$transactionDate]);

            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationReportDataByUploadID($uploadID);
            $transactionDate = date("Y-m-d", strtotime($date));
        }

        if ($type == 'AGRO'){
            $headingMsg = 'NOURISH AGRO LIMITED';
        }
        if ($type == 'POULTRY'){
            $headingMsg = 'NOURISH POULTRY & HATCHERY LIMITED';
        }
        if ($type == 'FEED'){
            $headingMsg = 'NOURISH FEEDS LIMITED';
        }

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-daily-report-pdf.html.twig',[
            'reconciliationData' => $reconciliationData,
            'headingMsg' => $headingMsg,
            'type' =>$type,
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('legal', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        if ($bank == null){
            $fileName = $type.'_'.$transactionDate.'_'.time().".pdf";
        }else{
            $bankData = $this->getDoctrine()->getRepository(Bank::class)->find($bank);
            $fileName = $bankData->getSlug().'_'.$type.'_'.$transactionDate.'_'.time().".pdf";
        }

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);

        die();
    }


//    Daily report statement TT
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/daily/report/statement/tt", name="br_reconciliation_daily_report_all_bank")
     */
    public function reconciliationDailyReprtStatementTT(Request $request){
        $form = $this->createForm(SearchFormType::class)->remove('bank','accountType');
        $form->handleRequest($request);

        $reconciliationData = [];
        $headingMsg = '';
        $type = '';
        $bank = '';
        $transactionDate = '';
        $accountCodeData = [];
        $bankSlug = '';
        if ($form->isSubmitted()){
            $date = $form['transactionDate']->getData();
            $transactionDate = date("Y-m-d", strtotime($date));
            $transactionDate = new \DateTime($date);

            if ($transactionDate){
                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationAllBank($transactionDate);
            }

            $bankSlug = [];
            foreach ($reconciliationData as $value){
                if(!in_array($value['bankSlug'], $bankSlug, true)){
                    array_push($bankSlug, $value['bankSlug']);
                }
            }

            $accountCodeData = $this->getDoctrine()->getRepository(BankAccountCode::class)->GetAllAccountCode();
        }

//        dd($accountCodeData,$reconciliationData);

        return $this->render('@TerminalbdBankReconciliation/report/reconciliation-daily-report-all-bank.html.twig',[
            'form' => $form->createView(),
            'reconciliationData' => $reconciliationData,
            'bankSlug' => $bankSlug,
            'transactionDate' => $transactionDate,
            'accountCodeData' => $accountCodeData,
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/daily/report/tt/excel", name="br_reconciliation_daily_report_tt_excel")
     */
    public function reconciliationDailyReprtTTExcel(Request $request){
        $date = $request->query->get('transactionDate');
        $transactionDate = date("Y-m-d", strtotime($date));
        $transactionDate = new \DateTime($date);

        if ($transactionDate){
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationAllBank($transactionDate);
        }

        $bankSlug = [];
        foreach ($reconciliationData as $value){
            if(!in_array($value['bankSlug'], $bankSlug, true)){
                array_push($bankSlug, $value['bankSlug']);
            }
        }
        $accountCodeData = $this->getDoctrine()->getRepository(BankAccountCode::class)->GetAllAccountCode();

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-daily-report-all-bank-tt-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'bankSlug' => $bankSlug,
            'transactionDate' => $transactionDate,
            'accountCodeData' => $accountCodeData,
        ]);
        $transactionDate = date("Y-m-d", strtotime($date));
        $fileName = 'Daily-report-tt-'.$transactionDate.'_'.time().".xls";

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$fileName");

        echo $html;
        die;
    }



    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/daily/report/tt/pdf", name="br_reconciliation_daily_report_tt_pdf")
     */
    public function reconciliationDailyReprtTTPdf(Request $request){
        $date = $request->query->get('transactionDate');
        $transactionDate = date("Y-m-d", strtotime($date));
        $transactionDate = new \DateTime($date);

        if ($transactionDate){
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationAllBank($transactionDate);
        }

        $bankSlug = [];
        foreach ($reconciliationData as $value){
            if(!in_array($value['bankSlug'], $bankSlug, true)){
                array_push($bankSlug, $value['bankSlug']);
            }
        }
        $accountCodeData = $this->getDoctrine()->getRepository(BankAccountCode::class)->GetAllAccountCode();


        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-daily-report-all-bank-tt-pdf.html.twig',[
            'reconciliationData' => $reconciliationData,
            'bankSlug' => $bankSlug,
            'transactionDate' => $transactionDate,
            'accountCodeData' => $accountCodeData,
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('legal', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        $transactionDate = date("Y-m-d", strtotime($date));
        $fileName = 'Daily-report-tt-'.$transactionDate.'_'.time().".pdf";

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);

        die();
    }



    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/daily/report/company/wise", name="br_reconciliation_daily_report_company_wise")
     */
    public function reconciliationDailyReprtCompanyWise(Request $request){
        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);

        $reconciliationData = [];
        $headingMsg = '';
        $type = '';
        $bank = '';
        $transactionDate = '';
        $companyData = '';
        if ($form->isSubmitted()){
            $date = $form['transactionDate']->getData();
            $transactionDate = date("Y-m-d", strtotime($date));
            $transactionDate = new \DateTime($date);
            $type = $form['accountType']->getData();
            $bank = $form['bank']->getData();

//            if ($bank == null && $type == null && !empty($transactionDate)){
//                $companyData = $this->getDoctrine()->getRepository(BankAccountCode::class)->CompanyWiseReconciliationData($transactionDate);
//
//                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->CompanyWiseTotalValue($transactionDate);
//            }
//            dd($companyData,$reconciliationData);

            if ($bank == null){
                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationReportData($type,$transactionDate);
            }else{
                $uploadID = $this->getDoctrine()->getRepository(\Proxies\__CG__\Terminalbd\BankReconciliationBundle\Entity\FileUpload::class)->findOneBy(['bank' => $bank, 'accountType' => $type,'transactionDate'=>$transactionDate]);

                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->getDailyReconciliationReportDataByUploadID($uploadID);
            }

            dd($reconciliationData);
//
            if ($type == 'AGRO'){
                $headingMsg = 'NOURISH AGRO LIMITED';
            }
            if ($type == 'POULTRY'){
                $headingMsg = 'NOURISH POULTRY & HATCHERY LIMITED';
            }
            if ($type == 'FEED'){
                $headingMsg = 'NOURISH FEEDS LIMITED';
            }
        }

        return $this->render('@TerminalbdBankReconciliation/report/reconciliation-daily-report-company-wise.html.twig',[
            'form' => $form->createView(),
            'companyData' => $reconciliationData,
            'headingMsg' => $headingMsg,
            'type'=> $type,
            'bank' => $bank,
            'transactionDate' => $transactionDate,
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/company/bank/report", name="br_reconciliation_company_bank_report")
     */
    public function reconciliationCompanyBankReprt(Request $request){
        $form = $this->createForm(DateRangeFormType::class);
        $form->handleRequest($request);

        $reconciliationData = [];
        $startDate = '';
        $endDate = '';
        if ($form->isSubmitted()){
            $startDate = $form['startDate']->getData();
            $startDate = date("Y-m-d", strtotime($startDate));
            $startDate = new \DateTime($startDate);

            $endDate = $form['endDate']->getData();
            $endDate = date("Y-m-d", strtotime($endDate));
            $endDate = new \DateTime($endDate);


            if (isset($startDate) && isset($endDate)){
                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationDataCompanyBankWise($startDate,$endDate);
            }
        }

        return $this->render('@TerminalbdBankReconciliation/report/reconciliation-company-bank-report.html.twig',[
            'form' => $form->createView(),
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/company/bank/report/excel", name="br_reconciliation_company_bank_report_excel")
     */
    public function reconciliationCompanyBankReprtExcel(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));

        if (isset($startDate) && isset($endDate)){
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationDataCompanyBankWise($startDate,$endDate);
        }

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-company-bank-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);

        $fileName = 'All_Company_Bank_'.$startDate.'_'.$endDate.".xls";

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$fileName");

        echo $html;
        die;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/company/bank/report/pdf", name="br_reconciliation_company_bank_report_pdf")
     */
    public function reconciliationCompanyBankReprtPDF(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));

        if (isset($startDate) && isset($endDate)){
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationDataCompanyBankWise($startDate,$endDate);
        }

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-company-bank-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('legal', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        $fileName = 'All_Company_Bank_'.$startDate.'_'.$endDate.".pdf";

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);

        die();
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/bank/report", name="br_reconciliation_bank_report")
     */
    public function reconciliationBankReprt(Request $request){
        $form = $this->createForm(DateRangeFormType::class);
        $form->handleRequest($request);

        $reconciliationData = [];
        $startDate = '';
        $endDate = '';
        if ($form->isSubmitted()){
            $startDate = $form['startDate']->getData();
            $startDate = date("Y-m-d", strtotime($startDate));
            $startDate = new \DateTime($startDate);

            $endDate = $form['endDate']->getData();
            $endDate = date("Y-m-d", strtotime($endDate));
            $endDate = new \DateTime($endDate);

            if (isset($startDate) && isset($endDate)){
                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationDataBankWise($startDate,$endDate);
            }
        }

        return $this->render('@TerminalbdBankReconciliation/report/reconciliation-bank-report.html.twig',[
            'form' => $form->createView(),
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/bank/report/excel", name="br_reconciliation_bank_report_excel")
     */
    public function reconciliationBankReportExcel(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));

        if (isset($startDate) && isset($endDate)){
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationDataBankWise($startDate,$endDate);
        }

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-bank-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);

        $fileName = 'All_Bank_'.$startDate.'_'.$endDate.".xls";

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$fileName");

        echo $html;
        die;
    }



    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/bank/report/pdf", name="br_reconciliation_bank_report_pdf")
     */
    public function reconciliationBankReprtPDF(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));

        if (isset($startDate) && isset($endDate)){
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationDataBankWise($startDate,$endDate);
        }

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-bank-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('legal', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        $fileName = 'All_Bank_'.$startDate.'_'.$endDate.".pdf";

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);

        die();
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/company/report", name="br_reconciliation_company_report")
     */
    public function reconciliationCompanyReport(Request $request){
        $form = $this->createForm(DateRangeFormType::class);
        $form->handleRequest($request);

        $reconciliationData = [];
        $startDate = '';
        $endDate = '';
        if ($form->isSubmitted()){
            $startDate = $form['startDate']->getData();
            $startDate = date("Y-m-d", strtotime($startDate));
            $startDate = new \DateTime($startDate);

            $endDate = $form['endDate']->getData();
            $endDate = date("Y-m-d", strtotime($endDate));
            $endDate = new \DateTime($endDate);

            if (isset($startDate) && isset($endDate)){
                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationDataCompanyWise($startDate,$endDate);
            }
        }

        return $this->render('@TerminalbdBankReconciliation/report/reconciliation-company-report.html.twig',[
            'form' => $form->createView(),
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/company/report/excel", name="br_reconciliation_company_report_excel")
     */
    public function reconciliationCompanyReportExcel(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));

        if (isset($startDate) && isset($endDate)){
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationDataCompanyWise($startDate,$endDate);
        }

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-company-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);

        $fileName = 'All_Company_'.$startDate.'_'.$endDate.".xls";

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$fileName");

        echo $html;
        die;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/company/report/pdf", name="br_reconciliation_company_report_pdf")
     */
    public function reconciliationCompanyReportPDF(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));

        if (isset($startDate) && isset($endDate)){
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationDataCompanyWise($startDate,$endDate);
        }

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-company-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('legal', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        $fileName = 'All_Company_'.$startDate.'_'.$endDate.".pdf";

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);

        die();
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/auto/report", name="br_reconciliation_auto_report")
     */
    public function reconciliationAutoReport(Request $request){
        $form = $this->createForm(DateRangeFormType::class);
        $form->handleRequest($request);

        $reconciliationData = [];
        $startDate = '';
        $endDate = '';
        $accountType = '';
        if ($form->isSubmitted()){
            $startDate = $form['startDate']->getData();
            $startDate = date("Y-m-d", strtotime($startDate));
            $startDate = new \DateTime($startDate);

            $endDate = $form['endDate']->getData();
            $endDate = date("Y-m-d", strtotime($endDate));
            $endDate = new \DateTime($endDate);

            $accountType = $form['accountType']->getData();

            $isCustom = 'no';
            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationAutoReport($startDate,$endDate,$isCustom,$accountType);
        }

        return $this->render('@TerminalbdBankReconciliation/report/reconciliation-auto-report.html.twig',[
            'form' => $form->createView(),
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'accountType' =>$accountType
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/auto/report/excel", name="br_reconciliation_auto_report_excel")
     */
    public function reconciliationAutoReportExcel(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));
        $accountType = $request->query->get('accountType');
        $isCustom = 'no';

        $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationAutoReport($startDate,$endDate,$isCustom,$accountType);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-auto-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'accountType' =>$accountType
        ]);

        $fileName = 'auto_reconciliation_'.$startDate.'_'.$endDate.".xls";

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$fileName");

        echo $html;
        die;
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/auto/report/pdf", name="br_reconciliation_auto_report_pdf")
     */
    public function reconciliationAutoReportPDF(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));
        $accountType = $request->query->get('accountType');
        $isCustom = 'no';

        $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationAutoReport($startDate,$endDate,$isCustom,$accountType);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-auto-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'accountType' =>$accountType
        ]);
        
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('legal', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();
        $fileName = 'auto_reconciliation_'.$startDate.'_'.$endDate.".pdf";

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);
        die();
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/custom/report", name="br_reconciliation_custom_report")
     */
    public function reconciliationCustomReport(Request $request){
        $form = $this->createForm(DateRangeFormType::class);
        $form->handleRequest($request);

        $reconciliationData = [];
        $startDate = '';
        $endDate = '';
        $accountType = '';
        if ($form->isSubmitted()){
            $startDate = $form['startDate']->getData();
            $startDate = date("Y-m-d", strtotime($startDate));
            $startDate = new \DateTime($startDate);

            $endDate = $form['endDate']->getData();
            $endDate = date("Y-m-d", strtotime($endDate));
            $endDate = new \DateTime($endDate);

            $accountType = $form['accountType']->getData();
            $isCustom = 'yes';

            $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationAutoReport($startDate,$endDate,$isCustom,$accountType);
        }

        return $this->render('@TerminalbdBankReconciliation/report/reconciliation-custom-report.html.twig',[
            'form' => $form->createView(),
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'accountType' =>$accountType
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/custom/report/excel", name="br_reconciliation_custom_report_excel")
     */
    public function reconciliationCustomReportExcel(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));
        $accountType = $request->query->get('accountType');
        $isCustom = 'yes';

        $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationAutoReport($startDate,$endDate,$isCustom,$accountType);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-custom-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'accountType' =>$accountType
        ]);

        $fileName = 'custom_reconciliation_'.$startDate.'_'.$endDate.".xls";

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$fileName");

        echo $html;
        die;
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation/custom/report/pdf", name="br_reconciliation_custom_report_pdf")
     */
    public function reconciliationCustomReportPDF(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));
        $accountType = $request->query->get('accountType');
        $isCustom = 'yes';

        $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->ReconciliationAutoReport($startDate,$endDate,$isCustom,$accountType);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/reconciliation-custom-report-excel.html.twig',[
            'reconciliationData' => $reconciliationData,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'accountType' =>$accountType
        ]);

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('legal', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();
        $fileName = 'custom_reconciliation_'.$startDate.'_'.$endDate.".pdf";

        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);
        die();
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/br/reset/reconciliation", name="br_reset_reconciliation")
     */
    public function resetReconciliation(Request $request){
        $em = $this->getDoctrine()->getManager();

        $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->find($_GET['reconciliationId']);
        $salesPaymentUpdate = $this->getDoctrine()->getRepository(SalesPayment::class)->find($reconciliationData->getSalesPayment());
        $bankTransaction = $this->getDoctrine()->getRepository(BankTransaction::class)->find($reconciliationData->getBankTransaction());

        if (isset($reconciliationData) && isset($salesPaymentUpdate)){
            $reconciliationData->setSalesPayment(null);
            if ($bankTransaction->getBranch() != null){
                $reconciliationData->setBranch($bankTransaction->getBranch());
            }
            $reconciliationData->setSalesPaymentAmount(null);
            $reconciliationData->setDiffAmount(null);
            $reconciliationData->setAgent(null);
            $reconciliationData->setActualAmount(null);
            $reconciliationData->setApprovedBy(null);
            $reconciliationData->setReconciliationFlag('teal');
            $reconciliationData->setIsCustom('yes');
            $em->persist($reconciliationData);
            $em->flush();

            $newReconciliation = new Reconciliation();
            $newReconciliation->setSalesPayment($salesPaymentUpdate);
            if ($salesPaymentUpdate->getBranch() != null){
                $newReconciliation->setBranch($salesPaymentUpdate->getBranch());
            }
            $newReconciliation->setStatus(1);
            $newReconciliation->setSalesPaymentAmount($salesPaymentUpdate->getDepositAmount());
            $newReconciliation->setAgent($salesPaymentUpdate->getAgent());
            $newReconciliation->setFileUpload($salesPaymentUpdate->getFileUpload());
            $newReconciliation->setActualAmount($salesPaymentUpdate->getDepositAmount());
            $newReconciliation->setReconciliationFlag('teal');
            $newReconciliation->setMode($reconciliationData->getMode());
            $newReconciliation->setIsCustom('yes');
            $em->persist($newReconciliation);
            $em->flush();

            $newEntity = $this->getDoctrine()->getRepository(Reconciliation::class)->find($newReconciliation->getId());
            $salesPaymentUpdate->setBankTransaction(null);
            $salesPaymentUpdate->setReconciliationId($newEntity);
            $em->persist($salesPaymentUpdate);
            $em->flush();
        }

        if ($newReconciliation){
            $bankId = $bankTransaction->getBank();
            $transactionDate = $bankTransaction->getTransactionDate();
            $fileUpload = $this->getDoctrine()->getRepository(FileUpload::class)->find($bankTransaction->getFileUpload());
            $accountType = $fileUpload->getAccountType();
            if ($bankId && $transactionDate && $accountType){
                $uploadID = $this->getDoctrine()->getRepository(FileUpload::class)->findOneBy(['bank' => $bankId, 'accountType' => $accountType,'transactionDate'=>$transactionDate]);

                $bank = $this->getDoctrine()->getRepository(Bank::class)->find($bankId);

                $salesSelect = $this->getDoctrine()->getRepository(SalesPayment::class)->getSalesDropDownData($bank,$transactionDate,$accountType);
                $reconciliationData = $this->getDoctrine()->getRepository(Reconciliation::class)->reconcialiationData($uploadID);
                $agentSelect = $this->getDoctrine()->getRepository(Agent::class)->findBy(['agentGroup' => 11,'status'=>1]);

                $html = $this->renderView(
                    '@TerminalbdBankReconciliation/reconciliation/load-data.html.twig', array(
                        'salesSelect' =>$salesSelect,
                        'reconciliationData'=>$reconciliationData,
                        'agentSelect'=>$agentSelect,
                    )
                );
            }

            return new JsonResponse([
                'message' => 'success',
//                'mobile' => $mobile,
                'contentHtml' => $html
            ]);
        }

    }
}