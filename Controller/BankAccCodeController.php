<?php


namespace Terminalbd\BankReconciliationBundle\Controller;

use App\Entity\Admin\Bank;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;
use Terminalbd\BankReconciliationBundle\Form\BankAccontCodeFormType;


/**
 * Class BankBranchController
 * @package Terminalbd\BankReconciliationBundle\Controller
 * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_USER')")
 */
class BankAccCodeController extends AbstractController
{
    private function paginate(Request $request, $records)
    {
        $paginator  = $this->get('knp_paginator');
        return $paginator->paginate($records, $request->query->get('page', 1)/*page number*/, 25  /*limit per page*/
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/account-code-list", name="br_account_code_list")
     */
    public function index(Request $request)
    {
//        $records = $this->getDoctrine()->getRepository(BankAccountCode::class)->findBy(['status'=>true],['id' => 'desc']);
        $records = $this->getDoctrine()->getRepository(BankAccountCode::class)->GetAccountCodeWithBranch();
        $data = $this->paginate($request, $records);
        return $this->render('@TerminalbdBankReconciliation/bankAccountCode/index.html.twig',[
            'data' => $data
        ]);
    }

    /**
     * @param Request $request
     * @Route("/account/code/create", name="br_account_code_create")
     */
    public function addAccountCode(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5120M');

        $code = new BankAccountCode();
        $form = $this->createForm(BankAccontCodeFormType::class,$code);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $accountType = $form['accountType']->getData();
            $accountNo = $form['accountNo']->getData();
            $bank = $form['bank']->getData();
            $accountCode = $form['accountCode']->getData();
            $status = $form['status']->getData();

//            dd($accountCode);


            $data = $request->request->all();
            $branchId = $data['bank_accont_code_form']['branchId'];
            $branch = $this->getDoctrine()->getRepository(BankBranch::class)->find($branchId);

            $dataExists = $this->getDoctrine()->getRepository(BankAccountCode::class)->findBy(['bank'=>$bank,'accountType'=>$accountType]);
            $countDataExists = count($dataExists);
            if ($countDataExists == 0){

                if (isset($accountCode)){
                    $existsCode = $this->getDoctrine()->getRepository(BankAccountCode::class)->findOneBy(['accountCode'=>$accountCode]);
                    $countExistsCode = count($existsCode);
                }else{
                    $countExistsCode = 0;
                }

                if ($countExistsCode == 0){
                    $code->setBank($bank);
                    $code->setBranch($branch);
                    $code->setAccountCode($accountCode);
                    $code->setAccountNo($accountNo);
                    $code->setAccountType($accountType);
                    $code->setStatus($status);
                    $code->setBranch($branch);
                    $em->persist($code);
                    $em->flush();

                    $this->addFlash('success', 'Account Code Added  Successfully !');
                    return $this->redirectToRoute('br_account_code_list');
                }else{
                    $this->addFlash('error', 'This Code already existing,Please try again !');
                    return $this->redirectToRoute('br_account_code_create');
                }
            }else{
                $this->addFlash('error', 'Account type already existing for this bank,Please try again !');
                return $this->redirectToRoute('br_account_code_create');
            }
        }

        return $this->render('@TerminalbdBankReconciliation/bankAccountCode/create.html.twig', [
            'form' => $form->createView(),
            'entity'=>null
        ]);
    }


    /**
     * @param Request $request
     * @Route("/branch/code/{id}/update", name="br_account_code_update")
     */
    public function AccountCodeUpdate(Request $request,BankAccountCode $code){
        $form = $this->createForm(BankAccontCodeFormType::class, $code);
        $form->handleRequest($request);


//        dd($code->getBranch());

        if ($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $accountType = $form['accountType']->getData();
            $accountNo = $form['accountNo']->getData();
            $bank = $form['bank']->getData();
            $accountCode = $form['accountCode']->getData();
            $status = $form['status']->getData();

            $data = $request->request->all();
            $branchId = $data['bank_accont_code_form']['branchId'];
            $branch = $this->getDoctrine()->getRepository(BankBranch::class)->find($branchId);

            $dataExists = $this->getDoctrine()->getRepository(BankAccountCode::class)->findBy(['bank'=>$bank,'accountType'=>$accountType]);
            $countDataExists = count($dataExists);
            if (($countDataExists == 0) || ($countDataExists == 1 && $code->getId() == $dataExists['0']->getId() )){
                if (isset($accountCode)){
                    $existsCode = $this->getDoctrine()->getRepository(BankAccountCode::class)->findBy(['accountCode'=>$accountCode]);
                    $countExistsCode = count($existsCode);
                }else{
                    $countExistsCode = 0;
                }
                
                if (($countExistsCode == 0) || ($countExistsCode == 1 && $code->getId() == $existsCode['0']->getId() )){
                    $code->setBank($bank);
                    $code->setBranch($branch);
                    $code->setAccountCode($accountCode);
                    $code->setAccountNo($accountNo);
                    $code->setAccountType($accountType);
                    $code->setStatus($status);
                    $code->setBranch($branch);
                    $em->persist($code);
                    $em->flush();

                    $this->addFlash('success', 'Account Code Update Successfully !');
                    return $this->redirectToRoute('br_account_code_list');
                }else{
                    $this->addFlash('error', 'This Code already existing,Please try again !');
                    return $this->redirectToRoute('br_account_code_create');
                }
            }else{
                $this->addFlash('error', 'Account type already existing for this bank,Please try again !');
                return $this->redirectToRoute('br_account_code_create');
            }
        }
        return $this->render('@TerminalbdBankReconciliation/bankAccountCode/create.html.twig',[
            'form' => $form->createView(),
            'entity' => $code
        ]);
    }
}