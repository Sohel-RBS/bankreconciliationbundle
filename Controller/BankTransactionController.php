<?php


namespace Terminalbd\BankReconciliationBundle\Controller;
use App\Entity\Admin\Bank;
use App\Entity\Core\Agent;
use Dompdf\Dompdf;
use Dompdf\Options;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\BankReconciliationBundle\Entity\BankAccountCode;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;
use Terminalbd\BankReconciliationBundle\Entity\BankTransaction;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Terminalbd\BankReconciliationBundle\Entity\Reconciliation;
use Terminalbd\BankReconciliationBundle\Entity\SalesPayment;
use Terminalbd\BankReconciliationBundle\Form\BranchWiseAgentFormType;
use Terminalbd\BankReconciliationBundle\Form\SearchFormType;
use Terminalbd\BankReconciliationBundle\Form\DateRangeFormType;
use Terminalbd\BankReconciliationBundle\Form\BankAccontCodeFormType;
use Symfony\Component\HttpFoundation\JsonResponse;


use App\Services\PostRequest;
use Symfony\Component\HttpFoundation\Request;
use App\Service\SmsSender;

/**
 * Class BankTransactionController
 * @package Terminalbd\BankReconciliationBundle\Controller
 * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_USER')")
 */
class BankTransactionController extends AbstractController
{
    private function paginate(Request $request, $records)
    {
        $paginator  = $this->get('knp_paginator');
        return $paginator->paginate($records, $request->query->get('page', 1)/*page number*/, 25  /*limit per page*/
        );
    }
    /**
     * @Route("/{id}/import", name="br_transaction_import")
     */
    public function import(FileUpload $file)
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        if ($file->isStatus() > 0){
            $this->addFlash('error','Data already imported!');
            return $this->redirectToRoute('br_bank_statement_upload');
        }
        $fileDir = $this->get('kernel')->getProjectDir() . '/public/uploads/reconciliation/';
        $status = $this->getDoctrine()->getRepository(BankTransaction::class)->insertData($file, $fileDir);

        if ($status){
//            $this->updateBranch($file);
            $this->addFlash('success','Data has been inserted successfully!');
        }else{
            $this->addFlash('error','Please follow recommended structure!');
        }
        return $this->redirectToRoute('br_bank_statement_list');
    }

    /**
     * @param FileUpload $file
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/{id}/branches-update", name="br_transaction_branch_update")
     */
    public function updateBranch(FileUpload $file)
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        $transactionDate = $file->getTransactionDate()->format('d-m-Y');
        $transactionBank = $file->getBank()->getId();
        $transactionAccountType = $file->getAccountType();
        $fileId = $file->getId();

        $branchUpdated = $this->getDoctrine()->getRepository(BankTransaction::class)->findOneBy(['fileUpload' => $file, 'status' => 1]);
//        if ($branchUpdated){
//            $this->addFlash('error', 'Branch already updated & Synchronisation completed!');
//            return $this->redirectToRoute('br_bank_statement_upload');
//        }

        $records = $this->getDoctrine()->getRepository(BankTransaction::class)->findBy(['fileUpload' => $file]);
        $branches = $this->getDoctrine()->getRepository(BankBranch::class)->getAllBranchesForSpecificBank($file);
//        dd($records,$branches);
        if ($records) {
//dd($file->getBank()->getSlug());
            if ($file->getBank()->getSlug() === 'bank-asia'){
                foreach ($records as $record){
                    $description = $record->getDescription();

                    $branchData = $this->branchNameToId($file->getBank(), $description);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'united-commercial-bank'){
                foreach ($records as $record){
                    $description = $record->getDescription();
                    $branchCode = substr($description, 0, 3);
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'ncc-bank'){
                foreach ($records as $record){
                    $description = $record->getDescription();
                    $branchCode = explode('Branch Code-', $description );
                    $branchCode = str_replace(' ', '', $branchCode[1]);
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'bangladesh-commerce-bank'){
                foreach ($records as $record){
                    $description = $record->getDescription();
                    $start = 'From ';
                    $end = ' BRANCH';
                    $branchName = $this->getBranchCodeBetween($description, $start, $end);
                    $wordCount = str_word_count($branchName);
                    if ($wordCount > 1){
                        $branchName = preg_replace('/\W\w+\s*(\W*)$/', '$1', $branchName);
                    }
                    $branchData = $this->branchNameToId($file->getBank(), $branchName);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'bangladesh-krishi-bank'){
                foreach ($records as $record){
                    $description = $record->getDescription();
//                    $branchCode = substr($description, strrpos($description, '##') + 2);
                    $branchData = $this->branchNameToId($file->getBank(), $description);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'union-bank'){
                foreach ($records as $record){
                    $description = $record->getDescription();
                    $start = 'SOURCE BR NAME: ';
                    $end = ' BRANCH';
                    $branchName = $this->getBranchCodeBetween($description, $start, $end);
                    if (empty($branchName)){
                        $description = $record->getDescription();
                        $start = 'SOURCE BR: ';
                        $end = ' BRANCH';
                        $branchName = $this->getBranchCodeBetween($description, $start, $end);
                    }

                    $branchData = $this->branchNameToId($file->getBank(), $branchName);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }
            if ($file->getBank()->getSlug() === 'national-bank'){
                foreach ($records as $record){
                    $transactionRef = $record->gettransactionRef();
                    $branchCode = substr($transactionRef, strpos($transactionRef, "\\") + 1);
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'Uttara-bank-ltd'){
                foreach ($records as $record){
//                    correction format
                    $description = $record->getDescription();

                    $branchData = $this->branchNameToId($file->getBank(), $description);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
//                    old format
//                    $description = $record->getDescription();
//                    $start = 'Rem Br : ';
//                    $end = '-';
//                    $branchCode = $this->getBranchCodeBetween($description, $start, $end);
//                    $branchCode = (int)$branchCode;
//
//                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
//                    if ($branchData){
//                        $record->setBranch($branchData);
//                        $record->setStatus(1);
//                        $this->getDoctrine()->getManager()->flush();
//                    }
                }
            }

            if ($file->getBank()->getSlug() === 'sonali-bank'){
                foreach ($records as $record){
                    $branchName = $record->getDescription();
                    $wordCount = str_word_count($branchName);
                    if ($wordCount > 1){
                        $branchName = preg_replace('/\W\w+\s*(\W*)$/', '$1', $branchName);
                    }

                    $branchData = $this->branchNameToId($file->getBank(), $branchName);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'janata-bank'){
                foreach ($records as $record){
//                    correction format
                    $transactionRef = $record->gettransactionRef();
                    $branchData = $this->branchNameToId($file->getBank(), $transactionRef);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
//                    old format
//                    $transactionRef = $record->gettransactionRef();
//                    $branchCode = substr($transactionRef, strpos($transactionRef, "\\") + 1);
//                    $branchCode = (int)$branchCode;
//                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
//                    if ($branchData){
//                        $record->setBranch($branchData);
//                        $record->setStatus(1);
//                        $this->getDoctrine()->getManager()->flush();
//                    }
                }
            }
            if ($file->getBank()->getSlug() === 'rupali-bank'){
                foreach ($records as $record){
                    $branchName = $record->getDescription();
                    $branchName = strtok($branchName, " ");
                    $branchData = $this->branchNameToId($file->getBank(), $branchName);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'ific-bank'){
                foreach ($records as $record){
                    $branchCode = $record->getDescription();
                    $branchData = $this->branchNameToId($file->getBank(), $branchCode);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'first-security-islami-bank'){
                foreach ($records as $record){
                    $description = $record->getDescription();
                    $start = 'Rem Br : ';
                    $end = '-';
                    $branchCode = $this->getBranchCodeBetween($description, $start, $end);
                    if (!$branchCode){
                        $start = 'FROM A/C NO. ';
                        $end = '-';
                        $branchCode = $this->getBranchCodeBetween($description, $start, $end);
                    }
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }


            if ($file->getBank()->getSlug() === 'agrani-bank' ){
                foreach ($records as $record){
                    $description = $record->gettransactionRef();
//                    dd($description);
//                    $start = '-';
//                    $end = ' ';
//                    $branchName = $this->getBranchCodeBetween($description, $start, $end);
                    $branchData = $this->branchNameToId($file->getBank(), $description);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'mercantile-bank'){
                foreach ($records as $record){
                    $transactionRef = $record->gettransactionRef();
                    $branchCode = substr($transactionRef, strpos($transactionRef, "\\") + 1);
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchCode){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'social-islami-bank'){
                foreach ($records as $record){
                    $description = $record->getDescription();
                    $start = 'SOURCE BR: ';
                    $end = ' Branch';
                    $branchName = $this->getBranchCodeBetween($description, $start, $end);
                    $branchData = $this->branchNameToId($file->getBank(), $branchName);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if (($file->getBank()->getSlug() === 'prime-bank-ltd') || ($file->getBank()->getSlug() === 'prime-bank') ){
                foreach ($records as $record){
                    $transactionRef = $record->gettransactionRef();
                    $branchCode = substr($transactionRef, strpos($transactionRef, "\\") + 1);
//                   Get first three character
                    $branchCode = substr($branchCode, 0, 3);
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchCode){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if($file->getBank()->getSlug() === 'shahjalal-islami-bank'){
                foreach ($records as $record){
                    $description = $record->getDescription();
                    $start = 'Rem Br : ';
                    $end = '-';
                    $branchCode = $this->getBranchCodeBetween($description, $start, $end);
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchCode){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'exim-bank'){
                foreach ($records as $record){
                    $transRef = $record->gettransactionRef();
                    $branchCode = substr($transRef, strpos($transRef, "\\") + 1);
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'pubali-bank' ){
                foreach ($records as $record){
                    $description = $record->getDescription();
                    $branchCode = strtok($description, '-');
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'dutch-bangla-bank'){
                foreach ($records as $record){
                    $description = $record->getInstrumentNo();
                    $branchCode = substr($description, strrpos($description, '#') + 1);
                    $branchData = $this->branchCodeToId($file->getBank(), $branchCode);
                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'brac-bank'){
                foreach ($records as $record){
                    $description = $record->getDescription();
                    $branchName = substr($description, strrpos($description, '#') + 1);
                    $branchData = $this->branchNameToId($file->getBank(), $branchName);

                    if ($branchData){
                        $record->setBranch($branchData);
                        $record->setStatus(1);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'al-arafah-islami-bank'){
                $start = '( ';
                $end = ' )';
                $accountNumber = 'SOURCE BR: ';

                foreach ($records as $record){
                    $description = $record->getDescription();
                    foreach ($branches as $branch) {
                        $branchCode = $this->getBranchCodeBetween($description, $start, $end);
                        if (!$branchCode) {
                            $branchCode = $this->getBranchCodeAccountNumber($description, $accountNumber);
                        }
                        if (strpos($description, $branch->getBranchName()) || $branchCode === $branch->getBranchCode()) {
                            $record->setBranch($branch);
                            $record->setStatus(1);
                            $this->getDoctrine()->getManager()->flush();
                        }
                    }
                }
            }

            if ($file->getBank()->getSlug() === 'islami-bank-bangladesh'){
                $start = 'Br.';
                $end = '-';
                $accountNumber = '2050';

                foreach ($records as $record) {
                    $description = $record->getDescription();

                    foreach ($branches as $branch) {
                        $branchCode = $this->getBranchCodeBetween($description, $start, $end);
                        if (!$branchCode) {
                            $branchCode = $this->getBranchCodeAccountNumber($description, $accountNumber);
                        }
                        if (strpos($description, $branch->getBranchName()) || $branchCode === $branch->getBranchCode()) {
                            $record->setBranch($branch);
                            $record->setStatus(1);
                            $this->getDoctrine()->getManager()->flush();
                        }
                    }
                }
            }
        }

//        dd($transactionDate,$transactionBank,$transactionAccountType,$fileId);

        return $this->redirectToRoute('br_sales_payment_sync_api', [
            'request-date' => $transactionDate,
            'bank-id' => $transactionBank,
            'receive-account' => $transactionAccountType,
            'file-upload-id' => $fileId
        ], 307);

        $this->addFlash('success', 'Branches have been updated!');
        return $this->redirectToRoute('br_bank_statement_upload');
    }

    public function branchCodeToId($bank, $branchCode){
        $branchData = $this->getDoctrine()
            ->getRepository(BankBranch::class)
            ->findOneBy([
                'bank' => $bank,
                'branchCode' => $branchCode,
            ]);
        return $branchData;
    }

    public function branchNameToId($bank, $branchName){
        $branchName = str_replace(',', '', $branchName);
        $branchName = str_replace('.', '', $branchName);
        $branchData=null;
        $searchBranchData = $this->getDoctrine()
            ->getRepository(BankBranch::class)
            ->getBranchNameByBankAndName($bank, $branchName);
        if(sizeof($searchBranchData)>0){
            $branchData=$searchBranchData[0];
        }
        return $branchData;
    }

    private function getBranchCodeBetween($description, $start, $end){
        $description = " ".$description;
        $ini = strpos($description,$start);
        if ($ini == 0){
            return null;
        }
        $ini += strlen($start);
        $len = strpos($description,$end,$ini) - $ini;

        return substr($description,$ini,$len);
    }

    private function getBranchCodeAccountNumber($description, $accountNumber){
        $description = " ".$description;
        $ini = strpos($description, $accountNumber);
        if ($ini){
            $ini += strlen($accountNumber);
            return substr($description, $ini,3);
        }else{
            return null;
        }

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/transaction/list", name="br_bank_transaction_list")
     */
    public function transactionList(Request $request)
    {
        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);
        $data = [];

        if ($form->isSubmitted()){
            $date = $form['transactionDate']->getData();
            $transactionDate = date("Y-m-d", strtotime($date));
            $transactionDate = new \DateTime($date);
            $type = $form['accountType']->getData();
            $bank = $form['bank']->getData();

            $uploadID = $this->getDoctrine()->getRepository(\Proxies\__CG__\Terminalbd\BankReconciliationBundle\Entity\FileUpload::class)->findOneBy(['bank' => $bank, 'accountType' => $type,'transactionDate'=>$transactionDate]);
            $records = $this->getDoctrine()->getRepository(BankTransaction::class)->findBy(['fileUpload'=>$uploadID],['id'=>'DESC']);
//            dd($records);
//            $data = $this->paginate($request, $records);
        }

//        dd($data);
        return $this->render('@TerminalbdBankReconciliation/transaction/index.html.twig',[
            'form' => $form->createView(),
//            'transactionData' => $data,
            'transactionData' => $records,
        ]);
    }

    /**
     * @Route("/transaction/{id}/delete", name="br_transaction_delete")
     */
    public function delete(BankTransaction $bankTransaction)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($bankTransaction);
        $em->flush();

        $this->addFlash('success', 'Transaction has been deleted!');
        return $this->redirectToRoute('br_bank_transaction_list');
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/sales/payment/list", name="br_sales_payment_list")
     */
    public function salesPaymentList(Request $request)
    {
        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);
        $data = [];

        if ($form->isSubmitted()){
            $date = $form['transactionDate']->getData();
            $transactionDate = date("Y-m-d", strtotime($date));
            $transactionDate = new \DateTime($date);
            $type = $form['accountType']->getData();
            $bank = $form['bank']->getData();

            $uploadID = $this->getDoctrine()->getRepository(\Proxies\__CG__\Terminalbd\BankReconciliationBundle\Entity\FileUpload::class)->findOneBy(['bank' => $bank, 'accountType' => $type,'transactionDate'=>$transactionDate]);

            $records = $this->getDoctrine()->getRepository(SalesPayment::class)->findBy(['fileUpload'=>$uploadID],['id'=>'DESC']);
            $data = $this->paginate($request, $records);
        }

        return $this->render('@TerminalbdBankReconciliation/transaction/sales-payment.html.twig',[
            'form' => $form->createView(),
            'transactionData' => $data,
        ]);
    }

    /**
     * @Route("/sales/payment/{id}/delete", name="br_sales_payment_delete")
     */
    public function salesPaymentDelete(SalesPayment $salesPayment)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($salesPayment);
        $em->flush();

        $this->addFlash('success', 'Sales Payment has been deleted!');
        return $this->redirectToRoute('br_sales_payment_list');
    }


    /**
     * @param BankTransaction $bankTransaction
     * @Route("/bank/instrumentno/{id}/inline/update", name="bank_instrumentno_inline_update")
     */
    public function inlineUpdateInstrumentNo(Request $request, BankTransaction $bankTransaction)
    {
        $data = $request->request->all();
        $bankTransaction->setInstrumentNo($data['value']);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['status' => 200]);
    }

    /**
     * @param BankTransaction $bankTransaction
     * @Route("/bank/deposit/{id}/inline/update", name="bank_deposit_inline_update")
     */
    public function inlineUpdateDeposit(Request $request, BankTransaction $bankTransaction)
    {
        $data = $request->request->all();
        $bankTransaction->setDeposit($data['value']);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['status' => 200]);
    }

    /**
     * @param BankTransaction $bankTransaction
     * @Route("/bank/description/{id}/inline/update", name="bank_description_inline_update")
     */
    public function inlineUpdateDescription(Request $request, BankTransaction $bankTransaction)
    {
        $data = $request->request->all();
        $bankTransaction->setDescription($data['value']);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['status' => 200]);
    }

    /**
     * @Route("/bank/branch/{id}/select", name="bank_branch_select")
     */
    public function selectBranch($id)
    {
        $findBranch = $this->getDoctrine()->getRepository(BankBranch::class)->find($id);

        $bankBranch = $this->getDoctrine()->getRepository(BankBranch::class)->getAllBranchForInlineEdit($findBranch->getBank());
        return New JsonResponse($bankBranch);
    }


    /**
     * @param BankTransaction $bankTransaction
     * @Route("/bank/branch/{id}/inline/update", name="bank_branch_inline_update")
     */
    public function inlineUpdateBranch(Request $request, BankTransaction $bankTransaction)
    {
        $data = $request->request->all();
        $BankBranch = $this->getDoctrine()->getRepository(BankBranch::class)->find($data['value']);
        $bankTransaction->setBranch($BankBranch);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['status' => 200]);
    }

    /**
     * @Route("/mode/select", name="mode_select")
     */
    public function selectMode()
    {
        $returnArray=[];
        $returnArray['Feed']='Feed';
        $returnArray['Chick']='Chick';
        return New JsonResponse($returnArray);
    }

    /**
     * @Route("/agent/select", name="agent_select")
     */
    public function selectAgent()
    {
        $findChickAgent = $this->getDoctrine()->getRepository(Agent::class)->findBy(['agentGroup' => 11,'status'=>1]);
        $returnArray=[];
        foreach ($findChickAgent as $result){
            $returnArray[$result->getId()]= $result->getName().' ('.$result->getAgentId().')';
        }
        return New JsonResponse($returnArray);
    }


    /**
     * @param Reconciliation $reconciliation
     * @Route("/bank/mode/{id}/inline/update", name="bank_mode_inline_update")
     */
    public function inlineUpdateMode(Request $request, Reconciliation $reconciliation)
    {
        $data = $request->request->all();
        $reconciliation->setMode($data['value']);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['status' => 200]);
    }

    /**
     * @param Reconciliation $reconciliation
     * @Route("/chick/agent/{id}/inline/update", name="chick_agent_inline_update")
     */
    public function inlineUpdateChickAgent(SmsSender $smsSender,Request $request, Reconciliation $reconciliation)
    {
        $em = $this->getDoctrine()->getManager();

        $data = $request->request->all();
        $findAgent = $this->getDoctrine()->getRepository(Agent::class)->find($data['value']);
        $transactionData = $this->getDoctrine()->getRepository(BankTransaction::class)->find($reconciliation->getBankTransaction());
        $chickSalesEntity = new SalesPayment();
        $chickSalesEntity->setAgent($findAgent);
        $chickSalesEntity->setBranch($transactionData->getBranch());
        $chickSalesEntity->setDepositAmount($reconciliation->getTransactionAmount());
        $chickSalesEntity->setDepositDate($transactionData->getTransactionDate());
        $chickSalesEntity->setStatus(true);
        $chickSalesEntity->setFileUpload($transactionData->getFileUpload());
        $chickSalesEntity->setReconciliationId($reconciliation);
        $chickSalesEntity->setBankTransaction($transactionData);
        $em->persist($chickSalesEntity);
        $em->flush();

        $reconciliation->setSalesPayment($chickSalesEntity);
        $reconciliation->setSalesPaymentAmount($reconciliation->getTransactionAmount());
        $reconciliation->setDiffAmount(0);
        $reconciliation->setAgent($findAgent);
        $reconciliation->setActualAmount($reconciliation->getTransactionAmount());
        $reconciliation->setReconciliationFlag('teal');
        $reconciliation->setApprovedBy($this->getUser());
        $reconciliation->setMode('Chick');
        $reconciliation->setIsCustom('yes');
        $em->persist($reconciliation);
        $em->flush();

        if ($findAgent->getMobile()){
//            $mobile = str_replace('+88', '', $findAgent->getMobile());
            $mobile = '01729762344';
            $message = 'Thanks! Payment received.';
//            $smsSender->sendSmsToAgent($message, $mobile);
        }
        return new JsonResponse(['status' => 200]);
    }


    /**
     * @Route("/bank/unrecognised/bank/transacton", name="br_unrecognised_bank_transacton")
     */
    public function unrecogisedBankTransaction(Request $request)
    {
        $form = $this->createForm(DateRangeFormType::class);
        $form->handleRequest($request);
        $records = [];
        $startDate = '';
        $endDate = '';
        $accountType = '';
        $bank = '';
        if ($form->isSubmitted()){
            $startDate = $form['startDate']->getData();
            $startDate = date("Y-m-d", strtotime($startDate));
            $startDate = new \DateTime($startDate);

            $endDate = $form['endDate']->getData();
            $endDate = date("Y-m-d", strtotime($endDate));
            $endDate = new \DateTime($endDate);

            $accountType = $form['accountType']->getData();
            $bank = $form['bank']->getData();

            $records = $this->getDoctrine()->getRepository(BankTransaction::class)->UnrecognisedBankTransacton($startDate,$endDate,$accountType,$bank);
        }

        return $this->render('@TerminalbdBankReconciliation/report/unrecognised/bank-transaction.html.twig',[
            'form' => $form->createView(),
            'data' => $records,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'accountType' => $accountType,
            'bank' => $bank,
        ]);
    }

    /**
     * @Route("/bank/unrecognised/bank/transacton/branch/agent", name="br_unrecognised_bank_transacton_branch_wise_agent")
     */
    public function unrecogisedBankTransactionBranchAgent(Request $request){
        $form = $this->createForm(BankAccontCodeFormType::class);
        $form->handleRequest($request);
        $records = [];
        $bank = '';
        $branch = '';
        $limit = '';
        if ($form->isSubmitted()){
            $bank = $form['bank']->getData();
            $data = $request->request->all();
            $branchId = $data['bank_accont_code_form']['branchId'];
            $limit = $data['limit'];
            if ($limit == null){
                $limit = 20;
            }

            $branch = $this->getDoctrine()->getRepository(BankBranch::class)->find($branchId);
            $records = $this->getDoctrine()->getRepository(Reconciliation::class)->UnrecognisedBankTransactonBranchAgent($branch,$limit);
        }

        return $this->render('@TerminalbdBankReconciliation/report/unrecognised/bank-transaction-branch-agent.html.twig',[
            'form' => $form->createView(),
            'data' => $records,
            'bank' => $bank,
            'branch' => $branch,
            'limit' => $limit,
            'entity'=>null
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/bank/transaction/unrecognised/report/excel", name="br_bank_transaction_unrecognised_report_excel")
     */
    public function unrecognisedDailyReprtExcel(Request $request){
//        dd('ok');
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));
        $accountType = $request->query->get('accountType');
        $bank = $request->query->get('bank');

        $records = $this->getDoctrine()->getRepository(BankTransaction::class)->UnrecognisedBankTransacton($startDate,$endDate,$accountType,$bank);

//        $records = $this->getDoctrine()->getRepository(BankTransaction::class)->UnrecognisedBankTransacton($startDate,$endDate,$accountType);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/unrecognised/bank-transaction-excel.html.twig',[
            'data' => $records,
            'accountType' => $accountType,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);

        $fileName = $accountType.'_'.$startDate.'_'.$endDate.'_'.time().".xls";

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$fileName");

        echo $html;
        die;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/bank/transaction/unrecognised/report/pdf", name="br_bank_transaction_unrecognised_report_pdf")
     */
    public function unrecognisedDailyReprtPdf(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));
        $accountType = $request->query->get('accountType');
        $bank = $request->query->get('bank');

        $records = $this->getDoctrine()->getRepository(BankTransaction::class)->UnrecognisedBankTransacton($startDate,$endDate,$accountType,$bank);



        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/unrecognised/bank-transaction-excel.html.twig',[
            'data' => $records,
            'accountType' => $accountType,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('legal', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        $fileName = $accountType.'_'.$startDate.'_'.$endDate.'_'.time().".pdf";


        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);

        die();
    }



    /**
     * @Route("/bank/unrecognised/sales/payment", name="br_unrecognised_sales_payment")
     */
    public function unrecogisedSalesPayment(Request $request)
    {
        $form = $this->createForm(DateRangeFormType::class);
        $form->handleRequest($request);
        $records = [];
        $startDate = '';
        $endDate = '';
        $accountType = '';
        $region = '';
        $agentid = '';
        if ($form->isSubmitted()){
            $startDate = $form['startDate']->getData();
            $startDate = date("Y-m-d", strtotime($startDate));
            $startDate = new \DateTime($startDate);

            $endDate = $form['endDate']->getData();
            $endDate = date("Y-m-d", strtotime($endDate));
            $endDate = new \DateTime($endDate);

            $region = $form['region']->getData();
            $agentid = $form['agentid']->getData();

            $records = $this->getDoctrine()->getRepository(SalesPayment::class)->UnrecognisedSalesPayment($startDate,$endDate,$region,$agentid);
        }


        return $this->render('@TerminalbdBankReconciliation/report/unrecognised/sales-payment.html.twig',[
            'form' => $form->createView(),
            'data' => $records,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'region' => $region,
            'agentId' => $agentid,
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/sales/payment/unrecognised/report/excel", name="br_sales_payment_unrecognised_report_excel")
     */
    public function salesPaymentDailyReprtExcel(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));
        $region = $request->query->get('region');
        $agentId = $request->query->get('agentId');

        $records = $this->getDoctrine()->getRepository(SalesPayment::class)->UnrecognisedSalesPayment($startDate,$endDate,$region,$agentId);


        $html = $this->renderView('@TerminalbdBankReconciliation/report/unrecognised/sales-payment-excel.html.twig',[
            'data' => $records,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);

        $fileName = $startDate.'_'.$endDate.'_'.time().".xls";

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$fileName");

        echo $html;
        die;
    }



    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/sales/payment/unrecognised/report/pdf", name="br_sales_payment_unrecognised_report_pdf")
     */
    public function salesPaymentReprtPdf(Request $request){
        $startDate = $request->query->get('startDate');
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = $request->query->get('endDate');
        $endDate = date("Y-m-d", strtotime($endDate));
        $region = $request->query->get('region');
        $agentId = $request->query->get('agentId');

        $records = $this->getDoctrine()->getRepository(SalesPayment::class)->UnrecognisedSalesPayment($startDate,$endDate,$region,$agentId);

        $html = $this->renderView('@TerminalbdBankReconciliation/report/unrecognised/sales-payment-excel.html.twig',[
            'data' => $records,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);



        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);


        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
        $dompdf->setPaper('legal', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        $fileName = $startDate.'_'.$endDate.'_'.time().".pdf";


        // Output the generated PDF to Browser (force download)
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);

        die();
    }
}