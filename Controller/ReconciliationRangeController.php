<?php


namespace Terminalbd\BankReconciliationBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BankReconciliationBundle\Entity\ReconciliationRange;
use Terminalbd\BankReconciliationBundle\Form\ReconciliationRangeFormType;

/**
 * Class ReconciliationRangeController
 * @package Terminalbd\BankReconciliationBundle\Controller
 * @Security("is_granted('ROLE_DEVELOPER') or is_granted('ROLE_BR_USER')")
 */
class ReconciliationRangeController extends AbstractController
{
    private function paginate(Request $request, $records)
    {
        $paginator  = $this->get('knp_paginator');
        return $paginator->paginate($records, $request->query->get('page', 1)/*page number*/, 25  /*limit per page*/
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/reconciliation-range", name="br_reconciliation_range")
     */
    public function index(Request $request, TranslatorInterface $translator){
        $ReconciliationRange = new ReconciliationRange();
        $form = $this->createForm(ReconciliationRangeFormType::class,$ReconciliationRange);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $startRange = $form['startRange']->getData();
            $endRange = $form['endRange']->getData();

            $ReconciliationRange->setEndRange($endRange);
            $ReconciliationRange->setStartRange($startRange);
            $ReconciliationRange->setStatus(1);
            $ReconciliationRange->setCreatedAt(new \DateTime('now'));

            $em->persist($ReconciliationRange);
            $em->flush();

            $this->addFlash('success', $translator->trans('New Range Added!'));
            return $this->redirectToRoute('br_reconciliation_range');
        }

        $data = $this->getDoctrine()->getRepository('TerminalbdBankReconciliationBundle:ReconciliationRange')->findBy(['status'=>1],['id' => 'desc']);

        return $this->render('@TerminalbdBankReconciliation/reconciliationRange/reconciliation-range.html.twig',[
            'form' => $form->createView(),
            'data' => $data,
        ]);
    }


    /**
     * @param Request $request
     * @Route("/reconciliation-range/{id}/update", name="br_reconciliation_range_update")
     */

    public function ReconciliationRangeUpdate(Request $request, ReconciliationRange $reconciliationRange){
        $form = $this->createForm(ReconciliationRangeFormType::class, $reconciliationRange);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $startRange = $form['startRange']->getData();
            $endRange = $form['endRange']->getData();

            $reconciliationRange->setStartRange($startRange);
            $reconciliationRange->setEndRange($endRange);
            $reconciliationRange->setUpdatedAt(new \DateTime('now'));
            $em->persist($reconciliationRange);
            $em->flush();


            $this->addFlash('success', 'Range Updated Successfully!');
            return $this->redirectToRoute('br_reconciliation_range');
        }

        $data = $this->getDoctrine()->getRepository('TerminalbdBankReconciliationBundle:ReconciliationRange')->findBy(['status'=>1],['id' => 'desc']);

        return $this->render('@TerminalbdBankReconciliation/reconciliationRange/reconciliation-range.html.twig',[
            'form' => $form->createView(),
            'data' => $data
        ]);
    }

    /**
     * @Route("/{id}/delete", name="br_reconciliation_range_delete")
     */
    public function delete(ReconciliationRange $reconciliationRange)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($reconciliationRange);
        $em->flush();

        $this->addFlash('success', 'Reconciliation Range has been deleted!');
        return $this->redirectToRoute('br_reconciliation_range');
    }

}