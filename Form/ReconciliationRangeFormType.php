<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Terminalbd\BankReconciliationBundle\Entity\ReconciliationRange;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ReconciliationRangeFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'attr' => [
                    'autocomplete' => 'off'
                ],
                'required' => true
            ])
            ->add('startRange', TextType::class,[
                'attr' => [
                    'autocomplete' => 'off'
                ],
                'required' => true
            ])
            ->add('endRange', TextType::class,[
                'attr' => [
                    'autocomplete' => 'off'
                ],
                'required' => true
            ])
            ->add('Submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReconciliationRange::class,
        ]);
    }


}