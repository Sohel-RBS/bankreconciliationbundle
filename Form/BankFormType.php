<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Terminalbd\BankReconciliationBundle\Entity\ReconciliationRange;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class BankFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'attr' => [
                    'autocomplete' => 'off'
                ],
                'required' => true
            ])

            ->add('deductionPercentage', TextType::class,[
                'attr' => [
                    'autocomplete' => 'off'
                ],
                'required' => false
            ])
//            ->add('status',CheckboxType::class,[
//                'required' => false,
//                'attr' => [
//                    'class' => 'checkboxToggle',
//                    'data-toggle' => "toggle",
//                    'data-style' => "slow",
//                    'data-offstyle' => "warning",
//                    'data-onstyle'=> "info",
//                    'data-on' => "Enabled",
//                    'data-off'=> "Disabled",
//                    'checked' => "checked"
//                ],
//            ])
            ->add('Submit', SubmitType::class)
            ->setMethod('post');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bank::class,
        ]);
    }


}