<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class FileUploadFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('bank', EntityType::class,[
                'placeholder' => '---Select Bank---',
                'class' => Bank::class,
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $er){
                return $er->createQueryBuilder('e')
                    ->where("e.status=1")
                    ->orderBy('e.name', 'ASC');
                },
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('transactionDate',DateType::class,[
                'widget' => 'single_text'
            ])
            ->add('accountType',ChoiceType::class,[
                'choices' => [
                    'Poultry' => 'POULTRY',
                    'Agro' => 'AGRO',
                    'Feeds' => 'FEED'
                ],
                'placeholder' => '---Select Account Type---',
            ])
            ->add('UploadFile', FileType::class, [
                'help' => 'Please upload only excel file!',
                'mapped' => false,
            ])
            ->add('Submit', SubmitType::class)
            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FileUpload::class,
        ]);
    }


}