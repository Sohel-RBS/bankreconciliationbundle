<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Terminalbd\BankReconciliationBundle\Entity\BankBranch;
use Terminalbd\BankReconciliationBundle\Entity\BankTransaction;
use Terminalbd\BankReconciliationBundle\Entity\FileUpload;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Terminalbd\BankReconciliationBundle\Entity\Reconciliation;
use Terminalbd\BankReconciliationBundle\Entity\SalesPayment;

class ReconciliationFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $bank = $options['fileUpload']->getBank()->getId();
        $builder
            ->add('branch', EntityType::class,[
                'placeholder' => '---Select Bank Branch---',
                'class' => BankBranch::class,
                'choice_label' => 'branchName',
                'query_builder' => function(EntityRepository $er)use($bank){
                    return $er->createQueryBuilder('e')
                        ->where("e.bank={$bank}")
                        ->orderBy('e.branchName', 'ASC');
                },
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('agentId',ChoiceType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'select2 agentSearch'
                ],
                'mapped' => false,
                'multiple' => false
            ])

            ->add('mode',ChoiceType::class,[
                'choices' => [
                    'Feed' => 'Feed',
                    'Chick' => 'Chick'
                ],
                'placeholder' => '---Select Mode---',
            ])
            ->add('Submit', SubmitType::class)
        ;
        $builder->add('bankTransaction', BankTransactionFormType::class,array());
        $builder->add('salesPayment', SalesPaymentFormType::class,array());

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reconciliation::class,
            'fileUpload' => FileUpload::class,
        ]);
    }


}