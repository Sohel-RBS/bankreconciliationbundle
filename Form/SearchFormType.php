<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace Terminalbd\BankReconciliationBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('bank', EntityType::class,[
                'class' => Bank::class,
                'placeholder' => 'Select Bank',
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $er){
                return $er->createQueryBuilder('e')
                    ->where('e.status = 1')
                    ->orderBy('e.name', 'ASC');
                },
                'attr' => [
                    'class' => 'select2'
                ],
                'required' => false
            ])
            ->add('transactionDate', TextType::class,[
                'attr' => [
                    'placeholder' => 'dd-mm-YYYY',
                    'autocomplete' => 'off'
                ],
                'required' => true
            ])
            ->add('accountType',ChoiceType::class,[
                'choices' => [
                    'Poultry' => 'POULTRY',
                    'Agro' => 'AGRO',
                    'Feeds' => 'FEED'
                ],
                'placeholder' => 'Select Account Type',
//                'attr' => [
//                    'class' => 'select2'
//                ],
                'required' => false
            ])
/*            ->add('branch', EntityType::class,[
                'class' => BankBranch::class,
                'required' => false,
                'placeholder' => 'Select branch',
                'choice_label' => 'branchName',
                'query_builder' => function(EntityRepository $er){
                return $er->createQueryBuilder('e')
                    ->where('e.status = -999');
                },
                'attr' => [
                    'class' => 'select2'
                ]
            ])*/
            ->add('Submit', SubmitType::class)
            ->setMethod('get')
            ;

/*        $builder->get('bank')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event){
                if(!empty($event->getForm()->getData())){
                    $bank = $event->getForm()->getData()->getId();
                    $this->fillBranch($event->getForm()->getParent(), $bank);
                }
            }

        );*/

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }


/*    public  function fillBranch(FormInterface $form, $bank) {
        $form->add('branch', EntityType::class, [
            'class' => BankBranch::class,
            'query_builder' => function (EntityRepository $er)  use($bank) {
                return $er->createQueryBuilder('e')
                    ->join('e.bank','bank')
                    ->where('e.status = 1')
                    ->andWhere("bank.id = {$bank}")
                    ->orderBy('e.branchName', 'ASC');
            },
            'attr'=>['class'=>'select2'],
            'choice_label' => 'branchName',
            'placeholder' => 'Select branch',
        ]);
    }*/


}